<?php

namespace App\Traits;

use App\Models\Pictures;

trait WithPictures
{
    public function picture(){
        return $this->morphOne(Pictures::class, 'pictureable')->latest()->withDefault();
    }

    public function pictures(){
        return $this->morphMany(Pictures::class, 'pictureable');
    }
}
