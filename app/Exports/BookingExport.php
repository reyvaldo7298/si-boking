<?php

namespace App\Exports;

use App\Models\Booking;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class BookingExport implements FromView, ShouldAutoSize, WithEvents, WithColumnFormatting
{
    use Exportable;

    private $bookings;

    public function __construct($bookings){
        $this->bookings = $bookings;
    }

    public function view(): View
    {
        return view('transaction.booking.report.booking', [
            'bookings' => $this->bookings,
        ]);

    }

    public function columnFormats(): array
    {
        return [
            'A' => '#',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ]
                ];
            },
        ];
    }
}
