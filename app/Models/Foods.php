<?php

namespace App\Models;

use App\Traits\WithPictures;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Foods extends Model
{
    use HasFactory, SoftDeletes, WithPictures;

    const FOOD = "Makanan";
    const DRINK = "Minuman";

    protected $fillable = [
        'name',
        'type',
        'price',
        'description'
    ];
}
