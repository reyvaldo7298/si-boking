<?php

namespace App\Models;

use App\Traits\WithPictures;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guest extends Model
{
    use HasFactory, SoftDeletes, WithPictures;

    protected $fillable = [
        'identity_number',
        'user_id',
        'name',
        'phone_number',
        'address'
    ];
}
