<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pictures extends Model
{
    use HasFactory;

    protected $fillable = [
        'pictureable_type',
        'pictureable_id',
        'file_name',
        'caption',
        'path'
    ];

    public function getUrlAttribute()
    {
        if (is_null($this->path)) {
            return asset('images/logo_watermark.jpg');
        }

        return asset('storage/'.$this->path.'/'.$this->file_name);
    }
}
