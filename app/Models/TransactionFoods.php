<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionFoods extends Model
{
    use HasFactory, SoftDeletes;

    const ORDER = "Dipesan";
    const PROCESS = "Proses";
    const DECLINE = "Ditolak";
    const FINISH = "Selesai";

    protected $fillable = [
        'transaction_id',
        'table_number',
        'reservation_date',
        'food_type',
        'food',
        'food_qty',
        'food_price',
        'total_price',
        'status',
        'gazeboo_id',
        'guest_id'
    ];

    public function guest(){
        return $this->hasOne(Guest::class, 'id', 'guest_id');
    }

    public function gazeboo(){
        return $this->hasOne(Gazeboos::class, 'id', 'gazeboo_id');
    }

    public function getStatusBadgeAttribute(){
        switch ($this->status){
            case self::ORDER :
                return "<span class='badge badge-warning'>".self::ORDER."</span>";
            case self::PROCESS :
                return "<span class='badge badge-secondary'>".self::PROCESS."</span>";
            case self::DECLINE :
                return "<span class='badge badge-danger'>".self::DECLINE."</span>";
            case self::FINISH :
                return "<span class='badge badge-success'>".self::FINISH."</span>";
            default :
                return "-";
        }
    }
}
