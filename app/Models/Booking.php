<?php

namespace App\Models;

use App\Traits\WithPictures;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use HasFactory, SoftDeletes, WithPictures;

    const PROCESS = "Proses";
    const BOOKED = "Booked";
    const DECLINE = "Ditolak";
    const FINISH = "Selesai";

    protected $fillable = [
        'booking_id',
        'status',
        'note',
        'booking_date',
        'booking_date_end',
        'booking_time',
        'booking_realization',
        'booking_time_approved',
        'gazeboo_id',
        'guest_id',
        'total_price',
        'booking_start',
        'booking_end'
    ];

    public function guest(){
        return $this->hasOne(Guest::class, 'id', 'guest_id')->withDefault();
    }

    public function gazeboo(){
        return $this->hasOne(Gazeboos::class, 'id', 'gazeboo_id');
    }

    public function serviceTime(){
        $start = Carbon::parse($this->created_at);
        $end = Carbon::parse($this->booking_time_approved);

        return $start->diffInMinutes($end).' Menit';
    }

    public function getStatusBadgeAttribute(){
        switch ($this->status){
            case self::PROCESS :
                return "<span class='badge badge-warning'>".self::PROCESS."</span>";
            case self::BOOKED :
                return "<span class='badge badge-secondary'>".self::BOOKED."</span>";
            case self::DECLINE :
                return "<span class='badge badge-danger'>".self::DECLINE."</span>";
            case self::FINISH :
                return "<span class='badge badge-success'>".self::FINISH."</span>";
            default :
                return "-";
        }
    }
}
