<?php

namespace App\Models;

use App\Traits\WithPictures;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gazeboos extends Model
{
    use HasFactory, SoftDeletes, WithPictures;

    const AVAILABLE = 1;
    const NOT_AVAILABLE = 0;

    const GAZEBOO_SINGLE = "Gazeboo Tunggal";
    const GAZEBOO_DOUBLE = "Gazeboo Gandeng";
    const LESEHAN = "Lesehan";
    const PENDOPO = "Pendopo";

    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    protected $fillable = [
        'number',
        'title',
        'description',
        'price',
        'category',
        'is_active',
        'is_available'
    ];

    public function bookings(){
        return $this->hasMany(Booking::class, 'gazeboo_id', 'id');
    }

    public function getActiveStatusAttribute(){
        return $this->is_active ? 'Aktif' : 'Non Aktif';
    }

    public function scopeActive($query){
        return $query->where('is_active', true);
    }

    public function getBookingStatusAttribute(){
        return $this->is_available ? 'Tersedia' : 'Booked';
    }

    public function scopeAvailable($query){
        return $query->where('is_available', true);
    }

    public function scopeBooked($query){
        return $query->where('is_available', false);
    }
}
