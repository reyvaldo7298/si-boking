<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Foods;
use App\Models\Gazeboos;
use App\Models\Guest;
use App\Models\TransactionFoods;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function cart()
    {
        if (Auth::user()){
            return view('cart.index');
        }else{
            return redirect()->route('login');
        }

    }

    public function addToCart($type, $id)
    {
        if ($type == 'Gazeboo'){
            $gazeboo = Gazeboos::findOrFail($id);

            $cart = session()->get('cart', []);

            if(isset($cart[$gazeboo->title]) && $cart[$gazeboo->title]["type"] == $gazeboo->category) {
                $cart[$gazeboo->title]['quantity']++;
            } else {
                $cart[$gazeboo->title] = [
                    "name" => $gazeboo->title,
                    "quantity" => 1,
                    "price" => $gazeboo->price,
                    "type" => $gazeboo->category,
                    "item_type" => $type
                ];
            }
        }

        if ($type == 'Food'){
            $food = Foods::findOrFail($id);

            $cart = session()->get('cart', []);

            if(isset($cart[$food->name]) && $cart[$food->name]["type"] == $food->type) {
                $cart[$food->name]['quantity']++;
            } else {
                $cart[$food->name] = [
                    "name" => $food->name,
                    "quantity" => 1,
                    "price" => $food->price,
                    "type" => $food->type,
                    "item_type" => $type
                ];
            }
        }

        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function update(Request $request)
    {
        if($request->id && $request->quantity){
            $cart = session()->get('cart');
            $cart[$request->id]["quantity"] = $request->quantity;
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if($request->id) {
            $cart = session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function order(Request $request){
        $validated = $request->validate([
            'booking_date_start' => 'required',
            'booking_date_end' => 'required',
        ]);

        $dateStart = Carbon::createFromFormat('Y-m-d', $request->booking_date_start)->toDateString();
        $dateEnd = Carbon::createFromFormat('Y-m-d', $request->booking_date_end)->toDateString();
        $diff = get_diff_days(Carbon::parse($dateStart)->subDay(), $dateEnd);

        $carts = session()->get('cart', []);

        $count_food = TransactionFoods::withTrashed()
            ->whereYear('created_at', Carbon::now()->format('Y'))
            ->whereMonth('created_at', Carbon::now()->format('m'))
            ->count();
        $transaction_id = "TRX-FOOD-".sprintf('%06d', $count_food);

        $guest = Guest::where('user_id', Auth::user()->id)->first();

        if (empty($guest)){
            return back();
        }

        $gazeboo_id = null;
        foreach ($carts as $cart){
            if ($cart['item_type'] == "Gazeboo"){
                $gazeboo = Gazeboos::where('title', $cart['name'])->first();
                $gazeboo_id = $gazeboo->id;
                $count_gazeboo = Booking::withTrashed()->where('gazeboo_id', $gazeboo_id)->count();
                $price = $gazeboo->price * ($diff);

                $booking_id = "GZB-".$gazeboo->number."-".sprintf('%06d', $count_gazeboo);

                $booking = new Booking();
                $booking['booking_id'] = $booking_id;
                $booking['status'] = Booking::PROCESS;
                $booking['note'] = '';
                $booking['booking_date'] = $dateStart;
                $booking['booking_date_end'] = $dateEnd;
                $booking['booking_time'] = $request->booking_time;
                $booking['gazeboo_id'] = $gazeboo_id;
                $booking['guest_id'] = $guest->id;
                $booking['total_price'] =$price;

                $booking->save();
            }
        }

        foreach ($carts as $cart){
            if ($cart['item_type'] == "Food" && $cart['type'] == "Makanan"){
                TransactionFoods::create([
                    'transaction_id' => $transaction_id,
                    'table_number' => null,
                    'reservation_date' => date("Y-m-d H:i:s", strtotime($dateStart)),
                    'status' => TransactionFoods::ORDER,
                    'gazeboo_id' => $gazeboo_id,
                    'guest_id' => $guest->id,
                    'food_type' => Foods::FOOD,
                    'food' => $cart['name'],
                    'food_qty' => $cart['quantity'],
                    'food_price' => $cart['quantity'] * $cart['price'],
                ]);
            }

            if ($cart['item_type'] == "Food" && $cart['type'] == "Minuman"){
                TransactionFoods::create([
                    'transaction_id' => $transaction_id,
                    'table_number' => null,
                    'reservation_date' => date("Y-m-d H:i:s", strtotime($dateStart)),
                    'status' => TransactionFoods::ORDER,
                    'gazeboo_id' => $request->gazeboo_id ?? null,
                    'guest_id' => 1,
                    'food_type' => Foods::DRINK,
                    'food' => $cart['name'],
                    'food_qty' => $cart['quantity'],
                    'food_price' => $cart['quantity'] * $cart['price'],
                ]);
            }
        }

        session()->forget('cart');
        return redirect()->back();
    }
}
