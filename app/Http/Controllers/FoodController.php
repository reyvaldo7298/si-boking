<?php

namespace App\Http\Controllers;

use App\Exports\FoodExport;
use App\Models\Foods;
use App\Models\Pictures;
use Illuminate\Http\Request;

class FoodController extends Controller
{
    public function index(){
        $indexLink = route('food.index');
        $createLink = route('food.create');

        $foods = Foods::latest()->paginate(10);

        return view('master.food.index', compact('indexLink', 'createLink', 'foods'));
    }

    public function create(){
        $indexLink = route('food.index');
        $storeLink = route('food.store');

        return view('master.food.create', compact('indexLink', 'storeLink'));
    }

    public function store(Request $request){
        $food = Foods::create($request->except('images'));

        insert_pictures($request->images, $food);

        return redirect()->route('food.index');
    }

    public function edit(Foods $food){
        $indexLink = route('food.index');
        $updateLink = route('food.update', $food);

        return view('master.food.edit', compact('updateLink', 'indexLink', 'food'));
    }

    public function update(Foods $food, Request $request){
        $food->update($request->all());

        if (!empty($request->images_old)){
            foreach ($request->images_old as $key => $image){
                $picture = Pictures::find($request->id_images_old[$key]);
                \Illuminate\Support\Facades\Storage::disk('public')->put($picture->path, $image);

                $picture->update([
                    'file_name' => $image->hashName(),
                ]);
            }
        }

        if (isset($request->images)){
            insert_pictures($request->images, $food);
        }

        return redirect()->route('food.index');
    }

    public function destroy(Foods $food){
        $food->delete();

        return redirect()->back();
    }
}
