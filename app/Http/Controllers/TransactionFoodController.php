<?php

namespace App\Http\Controllers;

use App\Exports\FoodExport;
use App\Models\Foods;
use App\Models\Gazeboos;
use App\Models\Guest;
use App\Models\TransactionFoods;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class TransactionFoodController extends Controller
{
    public function index(){
        $indexLink = route('transaction.food.index');
        $exportLink = route('food.export');
        $createLink = route('transaction.food.create');

        $array = TransactionFoods::all()->sortByDesc('transaction_id')->groupBy('transaction_id');

        $foods = $this->paginate($array);

        return view('transaction.food.index', compact('indexLink', 'exportLink', 'createLink', 'foods'));
    }

    public function paginate($items, $perPage = 10, $page = null)
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, [
            'path' => Paginator::resolveCurrentPath()
        ]);
    }


    public function create(){
        $indexLink = route('transaction.food.index');
        $storeLink = route('transaction.food.store');

        $gazeboes = Gazeboos::active()->booked()->get();
        $foods = Foods::where('type', Foods::FOOD)->get();
        $drinks = Foods::where('type', Foods::DRINK)->get();

        return view('transaction.food.create', compact('indexLink', 'storeLink', 'gazeboes', 'foods', 'drinks'));
    }

    public function store(Request $request){
        $request->merge(['reservation_date' => date("Y-m-d H:i:s", strtotime($request->booking_date))]);

        $count = TransactionFoods::withTrashed()
            ->whereYear('created_at', Carbon::now()->format('Y'))
            ->whereMonth('created_at', Carbon::now()->format('m'))
            ->count();
        $transaction_id = "TRX-FOOD-".sprintf('%06d', $count);

        $guest = Guest::create([
            'identity_number' => "-",
            'name' => $request->name,
            'phone_number' => "-",
            'address' => "-"
        ]);

        if (!empty($request->food)){
            for ($i = 0; $i < count($request->food); $i++){
                if (isset($request->food[$i])){
                    TransactionFoods::create([
                        'transaction_id' => $transaction_id,
                        'table_number' => $request->table_number,
                        'reservation_date' => $request->reservation_date,
                        'status' => TransactionFoods::ORDER,
                        'gazeboo_id' => $request->gazeboo_id ?? null,
                        'guest_id' => $guest->id,
                        'food_type' => Foods::FOOD,
                        'food' => $request->food[$i],
                        'food_qty' => (int)$request->food_qty[$i],
                        'food_price' => (int)$request->food_price[$i],
                    ]);
                }
            }
        }

        if (!empty($request->drink)){
            for ($i = 0; $i < count($request->drink); $i++){
                if (isset($request->drink[$i])){
                    TransactionFoods::create([
                        'transaction_id' => $transaction_id,
                        'table_number' => $request->table_number,
                        'reservation_date' => $request->reservation_date,
                        'status' => TransactionFoods::ORDER,
                        'gazeboo_id' => $request->gazeboo_id ?? null,
                        'guest_id' => $guest->id,
                        'food_type' => Foods::DRINK,
                        'food' => $request->drink[$i],
                        'food_qty' => $request->drink_qty[$i],
                        'food_price' => $request->drink_price[$i],
                    ]);
                }
            }
        }

        return redirect()->route('transaction.food.index');
    }

    public function destroy ($trx_id){
        TransactionFoods::where('transaction_id', $trx_id)->delete();

        return redirect()->back();
    }

    public function approve ($trx_id){
        TransactionFoods::where('transaction_id', $trx_id)
            ->update(
                ['status' => TransactionFoods::PROCESS]
            );

        return redirect()->back();
    }

    public function decline ($trx_id){
        TransactionFoods::where('transaction_id', $trx_id)
            ->update(
                ['status' => TransactionFoods::DECLINE]
            );

        return redirect()->back();
    }

    public function finish ($trx_id){
        TransactionFoods::where('transaction_id', $trx_id)
            ->update(
                ['status' => TransactionFoods::FINISH]
            );

        return redirect()->back();
    }

    public function Export()
    {
        $foods = TransactionFoods::all()->sortByDesc('transaction_id')->groupBy('transaction_id');
        $data = new FoodExport($foods);
        return Excel::download($data, 'Makanan & Minuman.xlsx');
    }
}
