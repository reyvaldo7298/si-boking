<?php

namespace App\Http\Controllers;

use App\Models\Gazeboos;
use App\Models\Pictures;
use Illuminate\Http\Request;

class GazebooController extends Controller
{
    public function index(){
        $indexLink = route('gazeboo.index');
        $createLink = route('gazeboo.create');

        $gazeboes = Gazeboos::latest()->paginate(10);

        return view('master.gazeboo.index', compact('indexLink', 'createLink', 'gazeboes'));
    }

    public function create(){
        $indexLink = route('gazeboo.index');
        $storeLink = route('gazeboo.store');

        return view('master.gazeboo.create', compact('indexLink', 'storeLink'));
    }

    public function store(Request $request){
        $gazeboo = Gazeboos::create($request->all());

        insert_pictures($request->images, $gazeboo);

        return redirect()->route('gazeboo.index');
    }

    public function edit(Gazeboos $gazeboo){
        $indexLink = route('gazeboo.index');
        $updateLink = route('gazeboo.update', $gazeboo);

        return view('master.gazeboo.edit', compact('updateLink', 'indexLink', 'gazeboo'));
    }

    public function update(Gazeboos $gazeboo, Request $request){
        $gazeboo->update($request->all());

        if (!empty($request->images_old)){
            foreach ($request->images_old as $key => $image){
                $picture = Pictures::find($request->id_images_old[$key]);
                \Illuminate\Support\Facades\Storage::disk('public')->put($picture->path, $image);

                $picture->update([
                    'file_name' => $image->hashName(),
                ]);
            }
        }

        if (isset($request->images)){
            insert_pictures($request->images, $gazeboo);
        }

        return redirect()->route('gazeboo.index');
    }

    public function destroy(Gazeboos $gazeboo){
        $gazeboo->delete();

        return redirect()->back();
    }

    public function deletePicture(Pictures $picture){
        $picture->delete();
        return back();
    }
}
