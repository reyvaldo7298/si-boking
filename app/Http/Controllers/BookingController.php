<?php

namespace App\Http\Controllers;

use App\Exports\BookingExport;
use App\Models\Booking;
use App\Models\Gazeboos;
use App\Models\Guest;
use App\Models\Pictures;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class BookingController extends Controller
{
    public function index(){
        $indexLink = route('booking.index');
        $exportLink = route('booking.export');
        $createLink = route('booking.create');

        $bookings = Booking::latest()->paginate(10);

        return view('transaction.booking.index', compact('indexLink', 'exportLink', 'createLink', 'bookings'));
    }

    public function create(){
        $indexLink = route('booking.index');
        $storeLink = route('booking.store');

        $gazeboes = Gazeboos::active()->available()->get();

        return view('transaction.booking.create', compact('indexLink', 'storeLink', 'gazeboes'));
    }

    public function store(Request $request){
        $date = get_start_and_end_date($request->booking_date);
        $dateStart = $date['start_date'];
        $dateEnd = $date['end_date'];

        $diff = get_diff_days(Carbon::parse($dateStart)->subDay(), $dateEnd);

        $request->merge(['booking_date' => date("Y-m-d H:i:s", strtotime($request->booking_date))]);
        $gazeboo = Gazeboos::find($request->gazeboo_id);
        $count = Booking::withTrashed()->where('gazeboo_id', $request->gazeboo_id)->count();
        $price = $gazeboo->price * $diff;

        $booking_id = "GZB-".$gazeboo->number."-".sprintf('%06d', $count);

        $request['booking_id'] = $booking_id;
        $request['status'] = Booking::PROCESS;

        $guest = Guest::create($request->all());

        $booking = new Booking();
        $booking['booking_id'] = $booking_id;
        $booking['status'] = Booking::PROCESS;
        $booking['note'] = $request->note;
        $booking['booking_date'] = $dateStart ?? Carbon::parse($request->booking_date);
        $booking['booking_date_end'] = $dateEnd;
        $booking['gazeboo_id'] = $request->gazeboo_id;
        $booking['guest_id'] = $guest->id;
        $booking['total_price'] =$price;

        $booking->save();

        if(!empty($request->images)){
            insert_pictures($request->images, $booking);
        }

        return redirect()->route('booking.index');
    }

    public function edit(Booking $booking){
        $indexLink = route('booking.index');
        $updateLink = route('booking.update', $booking);

        $gazeboes = Gazeboos::active()->available()->get();

        return view('transaction.booking.edit', compact('indexLink', 'updateLink', 'gazeboes', 'booking'));
    }

    public function update(Request $request, Booking $booking){
        $guest = Guest::find($booking->guest->id);
        $guest->update($request->all());

        $booking->update($request->all());

        if (!empty($request->images_old)){
            foreach ($request->images_old as $key => $image){
                $picture = Pictures::find($request->id_images_old[$key]);
                \Illuminate\Support\Facades\Storage::disk('public')->put($picture->path, $image);

                $picture->update([
                    'file_name' => $image->hashName(),
                ]);
            }
        }

        if (isset($request->images)){
            insert_pictures($request->images, $booking);
        }

        return redirect()->route('booking.index');
    }

    public function destroy(Booking $booking){
        $booking->delete();

        return redirect()->back();
    }

    public function finish(Booking $booking){
        $booking->update([
            'status' => Booking::FINISH,
            'booking_end' => Carbon::now()
        ]);
        $booking->gazeboo()->update(['is_available' => 1]);

        return redirect()->back();
    }

    public function realization(Booking $booking){
        $booking->update([
            'booking_realization' => Carbon::now()
        ]);

        return redirect()->back();
    }

    public function approve(Booking $booking){
        $booking->update([
            'booking_time_approved' => Carbon::now(),
            'status' => Booking::BOOKED,
            'booking_start' => Carbon::now()
        ]);
        $booking->gazeboo()->update(['is_available' => 0]);

        return redirect()->back();
    }

    public function decline(Booking $booking){
        $booking->update(['status' => Booking::DECLINE]);

        return redirect()->back();
    }

    public function Export()
    {
        $bookings = Booking::all();
        $data = new BookingExport($bookings);
        return Excel::download($data, 'Booking.xlsx');
    }
}
