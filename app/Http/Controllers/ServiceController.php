<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function index(){
        $bookings = Booking::latest()
            ->whereNotNull('booking_time_approved')
            ->paginate(10);

        return view('service.index', compact('bookings'));
    }
}
