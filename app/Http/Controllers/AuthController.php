<?php

namespace App\Http\Controllers;
use App\Models\Guest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        if(auth()->attempt(array('username' => $input['username'], 'password' => $input['password'])))
        {
            if (Auth::user()->is_admin){
                return redirect()->route('home');
            }
            return redirect()->route('landingPage');
        }else{
            return redirect()->route('login')->withInput()->with('error','Username/Password Salah.');
        }
    }

    public function register(){
        return view('register');
    }

    public function registerAccount(Request $request){
        $validated = $request->validate([
            'username' => 'unique:users,username',
        ]);

        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'password' => Hash::make($request->password),
        ]);

        $guest = Guest::create([
            'identity_number' => $request->identity_number,
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
            'user_id' => $user->id,
        ]);


        auth()->attempt(array('username' => $request->username, 'password' => Hash::make($request->password)));
        return redirect()->route('landingPage');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return redirect('/');
    }
}
