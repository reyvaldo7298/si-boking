<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Illuminate\Http\Request;

class ReportGazebooController extends Controller
{
    public function finish(){
        $bookings = Booking::latest()->where('status', Booking::FINISH)->paginate(10);

        return view('report.booking.finish', compact('bookings'));
    }

    public function decline(){
        $bookings = Booking::latest()->where('status', Booking::DECLINE)->paginate(10);

        return view('report.booking.decline', compact('bookings'));
    }
}
