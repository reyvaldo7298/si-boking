<?php

namespace App\Http\Controllers;

use App\Models\Foods;
use App\Models\Gazeboos;
use Illuminate\Http\Request;

class LandingPageController extends Controller
{

    public function index()
    {
        $foods = Foods::where('type', 'makanan')->latest()->get();
        $drinks = Foods::where('type', 'minuman')->latest()->get();
        $gazeboos = Gazeboos::Available()->Active()->latest()->get();

        return view('index', compact('foods', 'drinks', 'gazeboos'));
    }

    public function detailGazeboo(){
        $gazeboo = Gazeboos::find(request('Gazeboo'));

        return view('detail-gazeboo', compact('gazeboo'));
    }
    public function detailFood(){
        $food = Foods::find(request('Food'));

        return view('detail-food', compact('food'));
    }

}
