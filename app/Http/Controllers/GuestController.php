<?php

namespace App\Http\Controllers;

use App\Models\Foods;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function index()
    {
        $foods = Foods::all();

        return view('guest.index', compact('foods'));
    }
}
