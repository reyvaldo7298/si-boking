<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Illuminate\Http\Request;

class ServerBusyController extends Controller
{
    public function index(){
        $bookings = Booking::whereNotNull(['booking_start','booking_end'])
        ->whereHas('guest')
        ->when(\request('month'), function ($query){
            $query->whereMonth('booking_start', \request('month'));
        })
        ->when(\request('year'), function ($query){
            $query->whereYear('booking_start', \request('year'));
        })->get();

        return view('server.index', compact('bookings'));
    }
}
