<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\GoogleLoginController;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LandingPageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LandingPageController::class, 'index'])->name('landingPage');
Route::get('/detail-gazeboo', [LandingPageController::class, 'detailGazeboo'])->name('detail-gazeboo');
Route::get('/detail-food', [LandingPageController::class, 'detailFood'])->name('detail-food');

Route::get('/login/google', [GoogleLoginController::class, 'redirectToProvider'])->name('login-google');
Route::get('/login/google/callback', [GoogleLoginController::class, 'handleProviderCallback']);

Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::get('/register', [AuthController::class, 'register'])->name('register');
Route::post('/register-account', [AuthController::class, 'registerAccount'])->name('register-account');
Route::post('/login-cek', [AuthController::class, 'login'])->name('login-cek');
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

Route::get('cart', [CartController::class, 'cart'])->name('cart');
Route::get('add-to-cart/{type}/{id}', [CartController::class, 'addToCart'])->name('add.to.cart');
Route::post('order', [CartController::class, 'order'])->name('cart.order');
Route::patch('update-cart', [CartController::class, 'update'])->name('update.cart');
Route::delete('remove-from-cart', [CartController::class, 'remove'])->name('remove.from.cart');

Route::group(['middleware' => ['auth']], function () {
    Route::get('guest', [GuestController::class, 'index'])->name('guest');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('home', [HomeController::class, 'index'])->name('home');

    Route::prefix('master')->group(function () {
        Route::prefix('gazeboes')->group(function (){
            Route::get('/', [\App\Http\Controllers\GazebooController::class, 'index'])->name('gazeboo.index');
            Route::get('/create', [\App\Http\Controllers\GazebooController::class, 'create'])->name('gazeboo.create');
            Route::post('/store', [\App\Http\Controllers\GazebooController::class, 'store'])->name('gazeboo.store');
            Route::delete('/delete/{gazeboo}', [\App\Http\Controllers\GazebooController::class, 'destroy'])->name('gazeboo.delete');
            Route::get('/edit/{gazeboo}', [\App\Http\Controllers\GazebooController::class, 'edit'])->name('gazeboo.edit');
            Route::post('/update/{gazeboo}', [\App\Http\Controllers\GazebooController::class, 'update'])->name('gazeboo.update');
            Route::get('/delete-picture/{picture}', [\App\Http\Controllers\GazebooController::class, 'deletePicture'])->name('gazeboo.delete-picture');
        });

        Route::prefix('foods')->group(function (){
            Route::get('/', [\App\Http\Controllers\FoodController::class, 'index'])->name('food.index');
            Route::get('/create', [\App\Http\Controllers\FoodController::class, 'create'])->name('food.create');
            Route::post('/store', [\App\Http\Controllers\FoodController::class, 'store'])->name('food.store');
            Route::delete('/delete/{food}', [\App\Http\Controllers\FoodController::class, 'destroy'])->name('food.delete');
            Route::get('/edit/{food}', [\App\Http\Controllers\FoodController::class, 'edit'])->name('food.edit');
            Route::post('/update/{food}', [\App\Http\Controllers\FoodController::class, 'update'])->name('food.update');
        });
    });

    Route::prefix('transaction')->group(function (){
        Route::prefix('booking')->group(function (){
            Route::get('/', [\App\Http\Controllers\BookingController::class, 'index'])->name('booking.index');
            Route::get('/create', [\App\Http\Controllers\BookingController::class, 'create'])->name('booking.create');
            Route::post('/store', [\App\Http\Controllers\BookingController::class, 'store'])->name('booking.store');
            Route::delete('/delete/{booking}', [\App\Http\Controllers\BookingController::class, 'destroy'])->name('booking.delete');
            Route::get('/edit/{booking}', [\App\Http\Controllers\BookingController::class, 'edit'])->name('booking.edit');
            Route::post('/update/{booking}', [\App\Http\Controllers\BookingController::class, 'update'])->name('booking.update');
            Route::get('/realization/{booking}', [\App\Http\Controllers\BookingController::class, 'realization'])->name('booking.realization');
            Route::get('/approve/{booking}', [\App\Http\Controllers\BookingController::class, 'approve'])->name('booking.approve');
            Route::get('/decline/{booking}', [\App\Http\Controllers\BookingController::class, 'decline'])->name('booking.decline');
            Route::get('/finish/{booking}', [\App\Http\Controllers\BookingController::class, 'finish'])->name('booking.finish');
            Route::get('/export', [\App\Http\Controllers\BookingController::class, 'export'])->name('booking.export');
        });

        Route::prefix('food')->group(function (){
            Route::get('/', [\App\Http\Controllers\TransactionFoodController::class, 'index'])->name('transaction.food.index');
            Route::get('/create', [\App\Http\Controllers\TransactionFoodController::class, 'create'])->name('transaction.food.create');
            Route::post('/store', [\App\Http\Controllers\TransactionFoodController::class, 'store'])->name('transaction.food.store');
            Route::delete('/delete/{trx_id}', [\App\Http\Controllers\TransactionFoodController::class, 'destroy'])->name('transaction.food.delete');
            Route::get('/approve/{trx_id}', [\App\Http\Controllers\TransactionFoodController::class, 'approve'])->name('transaction.food.approve');
            Route::get('/decline/{trx_id}', [\App\Http\Controllers\TransactionFoodController::class, 'decline'])->name('transaction.food.decline');
            Route::get('/finish/{trx_id}', [\App\Http\Controllers\TransactionFoodController::class, 'finish'])->name('transaction.food.finish');
            Route::get('/export', [\App\Http\Controllers\TransactionFoodController::class, 'export'])->name('food.export');
        });
    });

    Route::prefix('/report')->group(function (){
        Route::prefix('/gazeboo')->group(function (){
            Route::get('/finish', [\App\Http\Controllers\ReportGazebooController::class, 'finish'])->name('report.gazeboo.finish');
            Route::get('/decline', [\App\Http\Controllers\ReportGazebooController::class, 'decline'])->name('report.gazeboo.decline');
        });
    });

    Route::get('/server-busyness', [\App\Http\Controllers\ServerBusyController::class, 'index'])->name('server-busyness');

    Route::get('/service', [\App\Http\Controllers\ServiceController::class, 'index'])->name('service.index');

});
