<?php

namespace Database\Seeders;

use App\Models\Guest;
use Illuminate\Database\Seeder;

class GuestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'identity_number'     => '5U35T',
                'name'  => 'Guest',
                'phone_number'  => "081246835357",
                'address'  => "Guest"
            ],
        ];

        foreach ($user as $key => $value) {
            Guest::create($value);
        }
    }
}
