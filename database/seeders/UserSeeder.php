<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name'     => 'Admin', 
                'username'  => 'admin',
                'password'  => bcrypt('123456'),
                'is_admin'  => 1
            ],
        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
