<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_foods', function (Blueprint $table) {
            $table->id();
            $table->string('transaction_id');
            $table->string('table_number')->nullable();
            $table->string('food_type')->nullable();
            $table->string('food')->nullable();
            $table->integer('food_qty')->default(0);
            $table->bigInteger('food_price')->default(0);
            $table->dateTime('reservation_date');
            $table->string('status');
            $table->bigInteger('gazeboo_id')->nullable();
            $table->bigInteger('guest_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_foods');
    }
}
