<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnBookingTimeInBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->time('booking_time')->after('booking_date_end')->nullable();
            $table->time('booking_realization')->after('booking_time')->nullable();
            $table->time('booking_time_approved')->after('booking_realization')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('booking_time');
            $table->dropColumn('booking_realization');
            $table->dropColumn('booking_time_approved');
        });
    }
}
