@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Booking</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-right mb-4">
                            </div>

                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr style="white-space: nowrap">
                                            <th scope="col">#</th>
                                            <th scope="col">Gazeboo</th>
                                            <th scope="col">Booking ID</th>
                                            <th scope="col">Pemesan</th>
                                            <th scope="col">No. Telp</th>
                                            <th scope="col">Alamat</th>
                                            <th scope="col">Keperluan</th>
                                            <th scope="col">Tanggal Booking</th>
                                            <th scope="col">Jam Kedatangan</th>
                                            <th scope="col">Jam Realisasi Kedatangan</th>
                                            <th scope="col">Total Bayar</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Bukti Booking</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($bookings as $key => $booking)
                                            <tr>
                                                <td>{{ $key + 1 + (((request('page') ?? 1) - 1) * 10) }}</td>
                                                <td>{{ $booking->gazeboo->number }}</td>
                                                <td><span style="white-space: nowrap">{{ $booking->booking_id }}</span></td>
                                                <td><span style="white-space: nowrap">{{ @$booking->guest->name }}</span></td>
                                                <td>{{ @$booking->guest->phone_number }}</td>
                                                <td>{{ @$booking->guest->address }}</td>
                                                <td>{{ $booking->note }}</td>
                                                <td style="white-space: nowrap">{{ parse_date($booking->booking_date) }}</td>
                                                <td style="white-space: nowrap">{{ parse_time_hm($booking->booking_time) }}</td>
                                                <td style="white-space: nowrap">{{ parse_time_hm($booking->booking_realization) }}</td>
                                                <td>{{ rp_format($booking->total_price) }}</td>
                                                <td class="text-center">{!! $booking->status_badge !!}</td>
                                                <td>
                                                    @foreach($booking->pictures as $picture)
                                                        <a href="{{ $picture->url }}" target="_blank"><img src="{{ $picture->url }}" class="img-thumbnail m-1" style="width:50px; display: inline-block"></a>
                                                    @endforeach
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="float-right">
                                {{ $bookings->appends(Request::except('page'))->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
