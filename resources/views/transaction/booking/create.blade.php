@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Tambah Booking</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            <form action="{{ $storeLink }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="gazeboo-id-input">Gazeboo</label>
                                            <select class="custom-select select2" id="gazeboo_id" name="gazeboo_id" required>
                                                @foreach($gazeboes as $gazeboo)
                                                    <option value="{{ $gazeboo->id }}">#{{ $gazeboo->number }} - {{ \Illuminate\Support\Str::limit($gazeboo->title, 20, '...') }} ({{ rp_format($gazeboo->price) }})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="booking-date-input">Tanggal Booking</label>
                                            <input name="booking_date" class="form-control" type="datetime-local" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name-input">Nama Pemesan</label>
                                            <input type="text" class="form-control" name="name" id="name-input" placeholder=""  autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="identity_number-input">Nomor KTP</label>
                                            <input type="text" class="form-control" name="identity_number" id="identity_number-input" placeholder=""  autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="phone_number-input">Nomor Telephon</label>
                                            <input type="text" class="form-control" name="phone_number" id="phone_number-input" placeholder=""  autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="address-input">Alamat</label>
                                        <textarea style="min-height: 50px" name="address" rows="10" class="form-control" id="address-input"  required></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="note-input">Keperluan</label>
                                        <textarea style="min-height: 120px" name="note" rows="10" class="form-control" id="note-input"  required></textarea>
                                    </div>

                                    <div class="form-group row">
                                        <label for="name-input">Bukti Booking</label>
                                        <div class="form-group col-xs-12 col-md-3">
                                            <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*" required/>
                                        </div>
                                        <div class="form-group col-xs-12 col-md-3">
                                            <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*"/>
                                        </div>
                                        <div class="form-group col-xs-12 col-md-3">
                                            <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*" />
                                        </div>
                                        <div class="form-group col-xs-12 col-md-3">
                                            <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*" />
                                        </div>
                                    </div>
                                </div>
                                <a href="{{ $indexLink }}" type="button" class="btn btn-danger"><i class="fas fa-times"></i> Batal</a>
                                <button type="submit" class="btn btn-success"><i class="fas fa-paper-plane"></i> Kirim</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('css')
    <link href="{{ asset('css/dropify.css') }}" rel="stylesheet">
@endpush
@push('javascript')
    <script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(function () {
            $('.dropify').dropify();
        });
    </script>

    <script>
        $('input[id="dates"]').daterangepicker();
    </script>
@endpush
