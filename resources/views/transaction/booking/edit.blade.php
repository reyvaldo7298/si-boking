@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Edit Booking : {{ ($booking->booking_id) }}</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            <form action="{{ $updateLink }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="gazeboo-id-input">Gazeboo</label>
                                            <select class="custom-select select2" id="gazeboo_id" name="gazeboo_id" required>
                                                @foreach($gazeboes as $gazeboo)
                                                    <option value="{{ $gazeboo->id }}" {{ $booking->gazeboo->id == $gazeboo->id ? 'selected':'' }}>#{{ $gazeboo->number }} - {{ \Illuminate\Support\Str::limit($gazeboo->title, 20, '...') }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="booking-date-input">Tanggal Booking</label>
                                            <input name="booking_date" class="form-control" type="datetime-local" value="{{ $booking->booking_date }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name-input">Nama Pemesan</label>
                                            <input type="text" class="form-control" name="name" id="name-input" placeholder="" value="{{ @$booking->guest->name }}" autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="identity_number-input">Nomor KTP</label>
                                            <input type="text" class="form-control" name="identity_number" id="identity_number-input" placeholder="" value="{{@ $booking->guest->identity_number }}"  autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="phone_number-input">Nomor Telephon</label>
                                            <input type="text" class="form-control" name="phone_number" id="phone_number-input" placeholder="" value="{{ @$booking->guest->phone_number }}"  autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="address-input">Alamat</label>
                                        <textarea style="min-height: 50px" name="address" rows="10" class="form-control" id="address-input" required>{{ $booking->guest->address }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="note-input">Keperluan</label>
                                        <textarea style="min-height: 120px" name="note" rows="10" class="form-control" id="note-input"  required>{{ $booking->note }}</textarea>
                                    </div>

                                    @for($x = 0; $x < count($booking->pictures); $x++)
                                        <div class="form-group col-xs-12 col-md-3">
                                            <input type="hidden" name="id_images_old[]" value="{{ $booking->pictures[$x]->id }}">
                                            <input type="file" name="images_old[{{ $x }}]" id="input-file-now" class="dropify" data-default-file="{{ $booking->pictures[$x]->url }}" accept="image/*"/>
                                            <a href="{{ route('gazeboo.delete-picture', $booking->pictures[$x]->id)}}" class="btn btn-danger btn-block mt-2">Hapus</a>
                                        </div>
                                    @endfor

                                    @for($i = count($booking->pictures); $i <= 3; $i++)
                                        <div class="form-group col-xs-12 col-md-3">
                                            <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*"/>
                                        </div>
                                    @endfor

                                </div>
                                <a href="{{ $indexLink }}" type="button" class="btn btn-danger"><i class="fas fa-times"></i> Batal</a>
                                <button type="submit" class="btn btn-success"><i class="fas fa-paper-plane"></i> Kirim</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('css')
    <link href="{{ asset('css/dropify.css') }}" rel="stylesheet">
@endpush
@push('javascript')
    <script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(function () {
            $('.dropify').dropify();
        });
    </script>
@endpush
