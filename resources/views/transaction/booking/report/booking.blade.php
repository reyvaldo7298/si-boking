<table>
    <thead>
    <tr>
        <th>#</th>
        <th>Gazeboo</th>
        <th>Booking ID</th>
        <th>Pemesan</th>
        <th>No. Telp</th>
        <th>Alamat</th>
        <th>Keperluan</th>
        <th>Tanggal Booking</th>
        <th>Total Bayar</th>
        <th>Status</th>
        <th>Bukti Booking</th>
    </tr>
    </thead>
    <tbody>
    @foreach($bookings as $key => $booking)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ @$booking->gazeboo->number }}</td>
            <td><span style="white-space: nowrap">{{ $booking->booking_id }}</span></td>
            <td><span style="white-space: nowrap">{{ @$booking->guest->name }}</span></td>
            <td>{{ $booking->guest->phone_number }}</td>
            <td>{{ $booking->guest->address }}</td>
            <td>{{ $booking->note }}</td>
            <td style="white-space: nowrap">{{ parse_date($booking->booking_date) }}</td>
            <td>{{ rp_format($booking->total_price) }}</td>
            <td class="text-center">{!! $booking->status_badge !!}</td>
            <td>
                @foreach($booking->pictures as $picture)
                    <a href="{{ $picture->url }}" target="_blank">Lampiran</a>
                @endforeach
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
