@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Menu</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-right mb-4">
                                <a href="{{ $exportLink }}" type="button" class="btn btn-primary"><i class="fas fa-file-excel"></i> Export</a>
                                <a href="{{ $createLink }}" type="button" class="btn btn-success"><i class="fas fa-plus"></i> Tambah</a>
                            </div>

                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr style="white-space: nowrap">
                                            <th scope="col">#</th>
                                            <th scope="col">Transaksi ID</th>
                                            <th scope="col">Pemesan</th>
                                            <th scope="col">Gazeboo</th>
                                            <th scope="col">Nomor Meja</th>
                                            <th scope="col">Makanan</th>
                                            <th scope="col">Minuman</th>
                                            <th scope="col">Total</th>
                                            <th scope="col">Tanggal Pemesanan</th>
                                            <th scope="col">Status</th>
                                            <th scope="col" class="text-center"><i class="fas fa-cog fa-spin"></i> Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($foods as $key => $food)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td><span style="white-space: nowrap">{{ $key }}</span></td>
                                            <td>{{ @$food->first()->guest->name }}</td>
                                            <td>{{ @$food->first()->gazeboo->id }}</td>
                                            <td>{{ @$food->first()->table_number }}</td>
                                            <td>
                                                @foreach($food->where('food_type', \App\Models\Foods::FOOD) as $fod)
                                                    <span style="white-space: nowrap">[{{ $fod->food_qty }}] {{ $fod->food }}</span> <br>
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach($food->where('food_type', \App\Models\Foods::DRINK) as $drink)
                                                    <span style="white-space: nowrap">[{{ $drink->food_qty }}] {{ $drink->food }}</span> <br>
                                                @endforeach
                                            </td>
                                            <td style="white-space: nowrap">{{ rp_format($food->sum('food_price')) }}</td>
                                            <td>{{ parse_date_time($food->first()->reservation_date) }}</td>
                                            <td>{!! $food->first()->status_badge !!}</td>
                                            <td style="white-space: nowrap">
                                                @if($food->first()->status == \App\Models\TransactionFoods::ORDER)
                                                    <a href="{{ route('transaction.food.approve',$key) }}" class="btn btn-success mb-1"><i class="fas fa-check"></i></a>
                                                    <a href="{{ route('transaction.food.decline',$key) }}" class="btn btn-danger mb-1"><i class="fas fa-times"></i></a>

                                                    <form action="{{ route('transaction.food.delete', $key) }}" method="POST" onsubmit="return confirm('Anda yakin menghapus data ini?')" style="display: inline !important;">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                                    </form>
                                                @endif

                                                @if(@$food->first()->status == \App\Models\TransactionFoods::PROCESS)
                                                    <a href="{{ route('transaction.food.finish',$key) }}" class="btn btn-success mb-1"><i class="fas fa-check"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="float-right">
{{--                                {{ $foods->appends(Request::except('page'))->links() }}--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
