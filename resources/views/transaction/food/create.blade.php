@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Tambah Menu</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ $storeLink }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row mb-4">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="gazeboo-id-input">Gazeboo</label>
                                            <select class="custom-select select2" id="gazeboo_id" name="gazeboo_id" >
                                                <option></option>
                                                @foreach($gazeboes as $gazeboo)
                                                    <option value="{{ $gazeboo->id }}">#{{ $gazeboo->number }} - {{ \Illuminate\Support\Str::limit($gazeboo->title, 20, '...') }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="table-number-input">Nomor Meja</label>
                                            <input type="text" class="form-control" name="table_number" id="table_number" placeholder="*Optional"  autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name-input">Nama Pemesan</label>
                                            <input type="text" class="form-control" name="name" id="name-input" placeholder=""  autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="booking-date-input">Tanggal Order</label>
                                            <input name="booking_date" class="form-control" type="datetime-local" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h4 class="float-left mt-1">Makanan</h4>

                                                <div class="float-right mb-4">
                                                    <button type="button" class="btn btn-danger" onclick="removeRowItemFoods()"><i class="fas fa-minus"></i></button>
                                                    <button type="button" class="btn btn-success" onclick="addRowItemFoods()"><i class="fas fa-plus"></i></button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row food_items">
                                            <input type="hidden" id="TotalFoods" value="0">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="food-input">Nama</label>
                                                    <select class="food custom-select select2" id="food" name="food[]" onchange="subTotalFoods()">
                                                        <option></option>
                                                        @foreach($foods as $food)
                                                            <option value="{{ $food->name }}" data-price="{{ $food->price }}">{{ $food->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="food-qty-input">Qty</label>
                                                    <input type="number" onchange="subTotalFoods()" min="0" class="food_qty form-control" name="food_qty[]" id="food_qty" placeholder="" value="1" autocomplete="off" required>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="food-price-input">Sub Harga</label>
                                                    <input type="number" min="0" class="food_price form-control" name="food_price[]" id="food_price" placeholder="" value="0" autocomplete="off" readonly>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="food_space"></div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h4 class="float-left mt-1">Minuman</h4>

                                                <div class="float-right mb-4">
                                                    <button type="button" class="btn btn-danger" onclick="removeRowItemDrinks()"><i class="fas fa-minus"></i></button>
                                                    <button type="button" class="btn btn-success" onclick="addRowItemDrinks()"><i class="fas fa-plus"></i></button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row drink_items">
                                            <input type="hidden" id="TotalDrinks" value="0">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="drink-input">Nama</label>
                                                    <select class="drink custom-select select2" id="drink" name="drink[]" onchange="subTotalDrinks()">
                                                        <option></option>
                                                        @foreach($drinks as $drink)
                                                            <option value="{{ $drink->name }}" data-price="{{ $drink->price }}">{{ $drink->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="drink-qty-input">Qty</label>
                                                    <input type="number" onchange="subTotalDrinks()" min="0" class="drink_qty form-control" name="drink_qty[]" id="drink_qty" placeholder="" value="1" autocomplete="off" required>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="drink-price-input">Sub Harga</label>
                                                    <input type="number" min="0" class="drink_price form-control" name="drink_price[]" id="drink_price" placeholder="" value="0" autocomplete="off" readonly>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="drink_space"></div>

                                    </div>
                                </div>

                                <div class="row mb-2 mt-1">
                                    <div class="col-md-12">
                                        <h5>Total Bayar : <span id="TotalPrice"></span></h5>
                                    </div>
                                </div>

                                <a href="{{ $indexLink }}" type="button" class="btn btn-danger"><i class="fas fa-times"></i> Batal</a>
                                <button type="submit" class="btn btn-success"><i class="fas fa-paper-plane"></i> Kirim</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('css')
    <link href="{{ asset('css/dropify.css') }}" rel="stylesheet">
@endpush
@push('javascript')
    <script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(function () {
            $('.dropify').dropify();
        });
    </script>

    <script>
        function subTotalFoods(){
            var sum = 0;
            $('.food_items').each(function (){
                var food_qty = $(this).find('.food_qty').val();
                var price = $(this).find(':selected').data('price');

                if(!isNaN(food_qty) && !isNaN(price)){
                    $(this).find('.food_price').val(food_qty * price);
                }

                sum += Number($(this).find('.food_price').val());
            });

            $('#TotalFoods').val(sum);

            totalPrice();
        }

        function subTotalDrinks(){
            var sum = 0;
            $('.drink_items').each(function (){
                var drink_qty = $(this).find('.drink_qty').val();
                var price = $(this).find(':selected').data('price');

                if(!isNaN(drink_qty) && !isNaN(price)){
                    $(this).find('.drink_price').val(drink_qty * price);
                }

                sum += Number($(this).find('.drink_price').val());
            });

            $('#TotalDrinks').val(sum);

            totalPrice();
        }

        function totalPrice(){
            var total_foods = $('#TotalFoods').val();
            var total_drinks = $('#TotalDrinks').val();
            var total = Number(total_foods) + Number(total_drinks);

            $('#TotalPrice').text(formatRupiah(Number(total)));
        }

        function formatRupiah(number){
            var format = new Intl.NumberFormat('id-ID', {
                style: 'currency',
                currency: 'IDR',
                minimumFractionDigits: 0,
            });

            return format.format(number);
        }

    </script>

    <script>
        function addRowItemFoods () {
            document.querySelector('#food_space').insertAdjacentHTML(
                'beforebegin',
                `
                    <div class="row items food_items" id="food_items">
                        <div class="col-md-5">
                            <div class="form-group">
                                <select class="food custom-select select2" id="food" name="food[]" onchange="subTotalFoods()">
                                    <option></option>
                                    @foreach($foods as $food)
                                        <option value="{{ $food->name }}" data-price="{{ $food->price }}">{{ $food->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="number" onchange="subTotalFoods()" min="0" class="food_qty form-control" name="food_qty[]" id="food_qty" placeholder="" value="1" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="number" min="0" class="food_price form-control" name="food_price[]" id="food_price" placeholder="" value="0" autocomplete="off" readonly>
                            </div>
                        </div>
                    </div>
                `
            )
        }

        function removeRowItemFoods () {
            $('.food_items').last().remove();
        }

        function addRowItemDrinks () {
            document.querySelector('#drink_space').insertAdjacentHTML(
                'beforebegin',
                `
                    <div class="row items drink_items" id="drink_items">
                        <div class="col-md-5">
                            <div class="form-group">
                                <select class="drink custom-select select2" id="food" name="drink[]" onchange="subTotalDrinks()">
                                    <option></option>
                                    @foreach($drinks as $drink)
                                        <option value="{{ $drink->name }}" data-price="{{ $drink->price }}">{{ $drink->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="number" onchange="subTotalDrinks()" min="0" class="drink_qty form-control" name="drink_qty[]" id="drink_qty" placeholder="" value="1" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="number" min="0" class="drink_price form-control" name="drink_price[]" id="drink_price" placeholder="" value="0" autocomplete="off" readonly>
                            </div>
                        </div>
                    </div>
                `
            )
        }

        function removeRowItemDrinks () {
            $('.drink_items').last().remove();
        }
    </script>

@endpush
