<table>
    <thead>
    <tr>
        <th>#</th>
        <th>Transaksi ID</th>
        <th>Pemesan</th>
        <th>Gazeboo</th>
        <th>Nomor Meja</th>
        <th>Makanan</th>
        <th>Minuman</th>
        <th>Total</th>
        <th>Tanggal Pemesanan</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($foods as $key => $food)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $key }}</td>
            <td>{{ @$food->first()->guest->name }}</td>
            <td>{{ @$food->first()->gazeboo->id }}</td>
            <td>{{ @$food->first()->table_number }}</td>
            <td>
                @foreach($food->where('food_type', \App\Models\Foods::FOOD) as $fod)
                    <span style="white-space: nowrap">[{{ $fod->food_qty }}] {{ $fod->food }}</span> <br>
                @endforeach
            </td>
            <td>
                @foreach($food->where('food_type', \App\Models\Foods::DRINK) as $drink)
                    <span style="white-space: nowrap">[{{ $drink->food_qty }}] {{ $drink->food }}</span> <br>
                @endforeach
            </td>
            <td style="white-space: nowrap">{{ rp_format($food->sum('food_price')) }}</td>
            <td>{{ parse_date_time($food->first()->reservation_date) }}</td>
            <td>{!! $food->first()->status_badge !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>
