@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Booking</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-dark">
                                    <tr style="white-space: nowrap">
                                        <th scope="col">#</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Tanggal</th>
                                        <th scope="col">Bulan</th>
                                        <th scope="col">Gazeboo</th>
                                        <th scope="col">Jam Kedatangan</th>
                                        <th scope="col">Waktu Pelayanan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $sum_arrival_time = 0;
                                        $sum_service_time = 0;

                                    ?>
                                    @foreach($bookings as $key => $booking)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $booking->guest->name }}</td>
                                            <td>{{ parse_date($booking->booking_start) }}</td>
                                            <td>{{ get_month_name(\Carbon\Carbon::parse($booking->booking_start)->month) }}</td>
                                            <td>{{ @$booking->gazeboo->number }}</td>
                                            <td>
                                                <?php
                                                    $hour =  \Carbon\Carbon::parse($booking->booking_start)->hour;
                                                    $minute =  \Carbon\Carbon::parse($booking->booking_start)->minute;

                                                    $calc_minute = $minute/60;
                                                    $total = round($hour+$calc_minute,2);

                                                    $sum_arrival_time = $total + $sum_arrival_time;
                                                    echo $total;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $startTime = \Carbon\Carbon::parse($booking->booking_start);
                                                    $finishTime = \Carbon\Carbon::parse($booking->booking_end);
                                                    $diff_service_time = $finishTime->diffInMinutes($startTime);
                                                    $sum_service_time = $diff_service_time + $sum_service_time;
                                                    echo $diff_service_time;
                                                ?>
                                            </td>
                                        </tr>
                                    @endforeach
                                        <tr>
                                            <?php
                                                $average_arrival = 0;
                                                $average_service = 0;
                                                $total_server = 0;

                                                if ($bookings->count() > 0) {
                                                    $average_arrival = $sum_arrival_time / $bookings->count();
                                                    $average_service = $sum_service_time / $bookings->count();
                                                    $total_server = $average_arrival / $average_service;
                                                }
                                            ?>
                                            <td colspan="5"></td>
                                            <td>{{ round($average_arrival,2) }}</td>
                                            <td>{{ round($average_service,2) }}</td>
                                        </tr>
                                    <tr>
                                        <td>Total kesibukan server : {{ $total_server }}</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
