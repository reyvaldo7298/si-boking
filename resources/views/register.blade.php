@extends('layouts.skeleton')
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2">
            <div class="login-brand">
{{--              <img src="https://img.celebrities.id/okz/300/p923nX/master_80s6QU9N7h_1834_arti_kata_masbro.jpg" alt="logo" width="100%">--}}
              <h4 class="mt-4">Sistem Informasi Pemesanan</h4>
            </div>

            <div class="card card-primary">
              <div class="card-header"><h4>Register</h4></div>
              <div class="card-body">
                @include('partials.message')
                <form method="POST" action="{{ route('register-account') }}" class="needs-validation row" novalidate="">
                @csrf
                  <div class="form-group col-6">
                    <label for="username">Username</label>
                    <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autofocus>
                    @error('username')
                    <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                  </div>

                    <div class="form-group col-6">
                        <div class="d-block">
                            <label for="password" class="control-label">Password</label>
                        </div>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>
                        @error('password')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>

                  <div class="form-group col-12">
                    <label for="name">Nama</label>
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required>
                    @error('name')
                    <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                  </div>

                  <div class="form-group col-6">
                    <label for="identity_number">Nomor KTP</label>
                    <input id="identity_number" type="text" class="form-control @error('identity_number') is-invalid @enderror" name="identity_number" value="{{ old('identity_number') }}" required>
                    @error('identity_number')
                    <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                  </div>

                  <div class="form-group col-6">
                    <label for="phone_number">Nomor Telp</label>
                    <input id="phone_number" type="text" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}" required>
                    @error('phone_number')
                    <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                  </div>

                  <div class="form-group col-12">
                    <label for="address">Alamat</label>
                    <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required>
                    @error('address')
                    <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                    {{ __('REGISTER') }}
                    </button>
                  </div>

                </form></div>
            </div>
            <div class="simple-footer">
              Copyright &copy; Janti Park {{ date('Y') }}
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>


