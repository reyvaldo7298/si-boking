@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Makanan & Minuman</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-right mb-4">
                                <a href="{{ $createLink }}" type="button" class="btn btn-success"><i class="fas fa-plus"></i> Tambah</a>
                            </div>

                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Deskripsi</th>
                                            <th scope="col">Harga</th>
                                            <th scope="col">Gambar</th>
                                            <th scope="col"><i class="fas fa-cog fa-spin"></i> Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($foods as $key => $food)
                                            <tr>
                                                <td>{{ $key + 1 + (((request('page') ?? 1) - 1) * 10) }}</td>
                                                <td>{{ $food->name }}</td>
                                                <td>{{ \Illuminate\Support\Str::limit($food->description, 100, '...') }}</td>
                                                <td>{{ rp_format($food->price) }}</td>
                                                <td>
                                                    @foreach($food->pictures as $picture)
                                                        <a href="{{ $picture->url }}" target="_blank"><img src="{{ $picture->url }}" class="img-thumbnail m-1" style="width:50px; display: inline-block"></a>
                                                    @endforeach
                                                </td>
                                                <td>
                                                    <a href="{{ route('food.edit',$food->id) }}" class="btn btn-warning mb-1"><i class="fas fa-pen"></i></a>
                                                    <form action="{{ route('food.delete', $food) }}" method="POST" onsubmit="return confirm('Anda yakin menghapus data ini?')" style="display: inline !important;">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="float-right">
                                {{ $foods->appends(Request::except('page'))->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
