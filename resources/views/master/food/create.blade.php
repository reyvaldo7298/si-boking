@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Tambah Makanan / Minuman</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            <form action="{{ $storeLink }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name-input">Nama</label>
                                            <input type="text" name="name" class="form-control" id="name-input" placeholder="" autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name-input">Jenis</label>
                                            <select class="custom-select select2" id="type" name="type" required>
                                                <option value="{{ \App\Models\Foods::FOOD }}">{{ \App\Models\Foods::FOOD }}</option>
                                                <option value="{{ \App\Models\Foods::DRINK }}">{{ \App\Models\Foods::DRINK }}</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="price-input">Harga</label>
                                            <input type="text" name="price" class="form-control" id="price-input" placeholder="" autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="description-input">Deskripsi</label>
                                        <textarea style="min-height: 120px" name="description" rows="10" class="form-control" id="description-input"  required></textarea>
                                    </div>

                                    <div class="form-group col-xs-12 col-md-3">
                                        <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*" required/>
                                    </div>
                                    <div class="form-group col-xs-12 col-md-3">
                                        <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*"/>
                                    </div>
                                    <div class="form-group col-xs-12 col-md-3">
                                        <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*" />
                                    </div>
                                    <div class="form-group col-xs-12 col-md-3">
                                        <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*" />
                                    </div>
                                </div>
                                <a href="{{ $indexLink }}" type="button" class="btn btn-danger"><i class="fas fa-times"></i> Batal</a>
                                <button type="submit" class="btn btn-success"><i class="fas fa-paper-plane"></i> Kirim</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('css')
    <link href="{{ asset('css/dropify.css') }}" rel="stylesheet">
@endpush
@push('javascript')
    <script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(function () {
            $('.dropify').dropify();
        });
    </script>
@endpush
