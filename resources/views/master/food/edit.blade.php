@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Edit {{ ucfirst($food->type) }} - {{ strtoupper($food->name) }}</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            <form action="{{ $updateLink }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name-input">Nama</label>
                                            <input type="text" name="name" class="form-control" id="name-input" placeholder="" value="{{ $food->name }}" autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name-input">Jenis</label>
                                            <select class="custom-select select2" id="type" name="type" required>
                                                <option value="{{ \App\Models\Foods::FOOD }}" {{ $food->type == \App\Models\Foods::FOOD ? 'selected' : '' }}>{{ \App\Models\Foods::FOOD }}</option>
                                                <option value="{{ \App\Models\Foods::DRINK }}" {{ $food->type == \App\Models\Foods::DRINK ? 'selected' : '' }}>{{ \App\Models\Foods::DRINK }}</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="price-input">Harga</label>
                                            <input type="number" name="price" class="form-control" id="price-input" placeholder="" value="{{ $food->price }}" autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="description-input">Deskripsi</label>
                                        <textarea style="min-height: 120px" name="description" rows="10" class="form-control" id="description-input"  required>{{ $food->description }} </textarea>
                                    </div>

                                    @for($x = 0; $x < count($food->pictures); $x++)
                                        <div class="form-group col-xs-12 col-md-3">
                                            <input type="hidden" name="id_images_old[]" value="{{ $food->pictures[$x]->id }}">
                                            <input type="file" name="images_old[{{ $x }}]" id="input-file-now" class="dropify" data-default-file="{{ $food->pictures[$x]->url }}" accept="image/*"/>
                                            <a href="{{ route('gazeboo.delete-picture', $food->pictures[$x]->id)}}" class="btn btn-danger btn-block mt-2">Hapus</a>
                                        </div>
                                    @endfor

                                    @for($i = count($food->pictures); $i <= 3; $i++)
                                        <div class="form-group col-xs-12 col-md-3">
                                            <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*"/>
                                        </div>
                                    @endfor

                                </div>
                                <a href="{{ $indexLink }}" type="button" class="btn btn-danger"><i class="fas fa-times"></i> Batal</a>
                                <button type="submit" class="btn btn-success"><i class="fas fa-paper-plane"></i> Kirim</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('css')
    <link href="{{ asset('css/dropify.css') }}" rel="stylesheet">
@endpush
@push('javascript')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(function () {
            $('.dropify').dropify();
        });
    </script>
@endpush
