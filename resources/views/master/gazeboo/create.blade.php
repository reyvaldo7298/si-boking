@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Tambah Gazeboo</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            <form action="{{ $storeLink }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="number-input">Nomor</label>
                                        <input type="text" class="form-control" name="number" id="number-input" placeholder=""  autocomplete="off" required>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="title-input">Judul</label>
                                        <input type="text" name="title" class="form-control" id="title-input" placeholder="" autocomplete="off" required>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="category-input">Kategori</label>
                                        <select class="custom-select select2" id="category" name="category" required>
                                            <option value="{{ \App\Models\Gazeboos::GAZEBOO_SINGLE }}">{{ \App\Models\Gazeboos::GAZEBOO_SINGLE }}</option>
                                            <option value="{{ \App\Models\Gazeboos::GAZEBOO_DOUBLE }}">{{ \App\Models\Gazeboos::GAZEBOO_DOUBLE }}</option>
                                            <option value="{{ \App\Models\Gazeboos::LESEHAN }}">{{ \App\Models\Gazeboos::LESEHAN }}</option>
                                            <option value="{{ \App\Models\Gazeboos::PENDOPO }}">{{ \App\Models\Gazeboos::PENDOPO }}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="price-input">Harga</label>
                                        <input type="text" class="form-control" name="price" id="price-input" placeholder=""  autocomplete="off" required>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="is-active-input">Status</label>
                                        <select class="custom-select select2" id="is_active" name="is_active" required>
                                            <option value="{{ \App\Models\Gazeboos::ACTIVE }}">{{ \App\Models\Gazeboos::ACTIVE == true ?  'Aktif' : 'Tidak Aktif' }}</option>
                                            <option value="{{ \App\Models\Gazeboos::NOT_ACTIVE }}">{{ \App\Models\Gazeboos::NOT_ACTIVE == true ?  'Aktif' : 'Tidak Aktif' }}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="description-input">Deskripsi</label>
                                    <textarea style="min-height: 120px" name="description" rows="10" class="form-control" id="description-input"  required></textarea>
                                </div>

                                <div class="form-group col-xs-12 col-md-3">
                                    <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*" required/>
                                </div>
                                <div class="form-group col-xs-12 col-md-3">
                                    <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*"/>
                                </div>
                                <div class="form-group col-xs-12 col-md-3">
                                    <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*" />
                                </div>
                                <div class="form-group col-xs-12 col-md-3">
                                    <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*" />
                                </div>


                        </div>
                                <a href="{{ $indexLink }}" type="button" class="btn btn-danger"><i class="fas fa-times"></i> Batal</a>
                                <button type="submit" class="btn btn-success"><i class="fas fa-paper-plane"></i> Kirim</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('css')
    <link href="{{ asset('css/dropify.css') }}" rel="stylesheet">
@endpush
@push('javascript')
    <script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(function () {
            $('.dropify').dropify();
        });
    </script>
@endpush
