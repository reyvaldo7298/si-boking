@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Edit Gazeboo #{{ strtoupper($gazeboo->number) }}</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            <form action="{{ $updateLink }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="number-input">Nomor</label>
                                            <input type="text" class="form-control" name="number" id="number-input" value="{{ $gazeboo->number }}" autocomplete="off" placeholder="" required>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="title-input">Judul</label>
                                            <input type="text" name="title" class="form-control" id="title-input" placeholder="" autocomplete="off" value="{{ $gazeboo->title }}"  required>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="category-input">Kategori</label>
                                            <select class="custom-select select2" id="category" name="category" required>
                                                <option @if($gazeboo->category == \App\Models\Gazeboos::GAZEBOO_SINGLE ) selected @endif value="{{ \App\Models\Gazeboos::GAZEBOO_SINGLE }}">{{ \App\Models\Gazeboos::GAZEBOO_SINGLE }}</option>
                                                <option @if($gazeboo->category == \App\Models\Gazeboos::GAZEBOO_DOUBLE ) selected @endif value="{{ \App\Models\Gazeboos::GAZEBOO_DOUBLE }}">{{ \App\Models\Gazeboos::GAZEBOO_DOUBLE }}</option>
                                                <option @if($gazeboo->category == \App\Models\Gazeboos::LESEHAN ) selected @endif value="{{ \App\Models\Gazeboos::LESEHAN }}">{{ \App\Models\Gazeboos::LESEHAN }}</option>
                                                <option @if($gazeboo->category == \App\Models\Gazeboos::PENDOPO ) selected @endif value="{{ \App\Models\Gazeboos::PENDOPO }}">{{ \App\Models\Gazeboos::PENDOPO }}</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="is-active-input">Status</label>
                                            <select class="custom-select select2" id="is_active" name="is_active" required>
                                                <option value="{{ \App\Models\Gazeboos::ACTIVE }}" {{ $gazeboo->is_active == \App\Models\Gazeboos::ACTIVE ? 'selected' : '' }}>{{ \App\Models\Gazeboos::ACTIVE == true ?  'Aktif' : 'Tidak Aktif' }}</option>
                                                <option value="{{ \App\Models\Gazeboos::NOT_ACTIVE }}" {{ $gazeboo->is_active == \App\Models\Gazeboos::NOT_ACTIVE ? 'selected' : '' }}>{{ \App\Models\Gazeboos::NOT_ACTIVE == true ?  'Aktif' : 'Tidak Aktif' }}</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="description-input">Deskripsi</label>
                                        <textarea style="min-height: 120px" name="description" rows="10" class="form-control" id="description-input"  required>{{ $gazeboo->description }} </textarea>
                                    </div>

                                    @for($x = 0; $x < count($gazeboo->pictures); $x++)
                                        <div class="form-group col-xs-12 col-md-3">
                                            <input type="hidden" name="id_images_old[]" value="{{ $gazeboo->pictures[$x]->id }}">
                                            <input type="file" name="images_old[{{ $x }}]" id="input-file-now" class="dropify" data-default-file="{{ $gazeboo->pictures[$x]->url }}" accept="image/*"/>
                                            <a href="{{ route('gazeboo.delete-picture', $gazeboo->pictures[$x]->id)}}" class="btn btn-danger btn-block mt-2">Hapus</a>
                                        </div>
                                    @endfor

                                    @for($i = count($gazeboo->pictures); $i <= 3; $i++)
                                        <div class="form-group col-xs-12 col-md-3">
                                            <input type="file" name="images[]" id="input-file-now" class="dropify" accept="image/*"/>
                                        </div>
                                    @endfor

                                </div>
                                <a href="{{ $indexLink }}" type="button" class="btn btn-danger"><i class="fas fa-times"></i> Batal</a>
                                <button type="submit" class="btn btn-success"><i class="fas fa-paper-plane"></i> Kirim</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('css')
    <link href="{{ asset('css/dropify.css') }}" rel="stylesheet">
@endpush
@push('javascript')
    <script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(function () {
            $('.dropify').dropify();
        });
    </script>
@endpush
