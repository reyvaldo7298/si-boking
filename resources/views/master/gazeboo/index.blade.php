@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Gazeboo</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-right mb-4">
                                <a href="{{ $createLink }}" type="button" class="btn btn-success"><i class="fas fa-plus"></i> Tambah</a>
                            </div>

                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Nomor</th>
                                            <th scope="col">Judul</th>
                                            <th scope="col">Kategori</th>
                                            <th scope="col">Harga</th>
                                            <th scope="col">Deskripsi</th>
                                            <th scope="col">Gambar</th>
                                            <th scope="col">Ketersediaan</th>
                                            <th scope="col">Status</th>
                                            <th scope="col"><i class="fas fa-cog fa-spin"></i> Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($gazeboes as $gazeboo)
                                            <tr>
                                                <td>{{ $gazeboo->number }}</td>
                                                <td>{{ \Illuminate\Support\Str::limit($gazeboo->title, 100, '...') }}</td>
                                                <td>{{ $gazeboo->category }}</td>
                                                <td>{{ rp_format($gazeboo->price) }}</td>
                                                <td>{{ \Illuminate\Support\Str::limit($gazeboo->description, 100, '...') }}</td>
                                                <td>
                                                    @foreach($gazeboo->pictures as $picture)
                                                        <a href="{{ $picture->url }}" target="_blank"><img src="{{ $picture->url }}" class="img-thumbnail m-1" style="width:50px; display: inline-block"></a>
                                                    @endforeach
                                                </td>
                                                <td>
                                                    <span class="badge badge-{{ $gazeboo->is_available ? 'success' : 'secondary'}}">{{ $gazeboo->booking_status }}</span>
                                                </td>
                                                <td>
                                                    <span class="badge badge-{{ $gazeboo->is_active ? 'success' : 'danger'}}">{{ $gazeboo->active_status }}</span>
                                                </td>
                                                <td>
                                                    <a href="{{ route('gazeboo.edit',$gazeboo->id) }}" class="btn btn-warning mb-1"><i class="fas fa-pen"></i></a>
                                                    <form action="{{ route('gazeboo.delete', $gazeboo) }}" method="POST" onsubmit="return confirm('Anda yakin menghapus data ini?')" style="display: inline !important;">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="float-right">
                                {{ $gazeboes->appends(Request::except('page'))->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
