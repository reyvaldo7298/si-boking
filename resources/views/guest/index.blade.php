@extends('layouts.app')

@section('title', 'Dashboard')

@push('css')
<link   rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>
@endpush

@section('content')
<section class="section">
    <div class="section-header">
      <h1>Carousel</h1>
    </div>

    <div class="section-body">

      <div class="row">

        @foreach ($foods as $food)
        <div class="col-12 col-md-6 col-lg-6">
          <div class="card">
            <div class="card-header">
              <h4>{{ $food->name }}</h4>
            </div>
            <div class="card-body">
                <div class="mt-6 mx-60 swiper mySwiper">
                    <div class="swiper-wrapper">
                        @foreach ($food->pictures as $key => $picture)
                        <div class="swiper-slide">
                            <img
                              class="object-cover w-full h-full"
                              src="{{ $picture->url }}"
                            />
                          </div>
                        @endforeach
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                </div>

              </div>
            </div>
          </div>
          @endforeach

      </div>
    </div>
  </section>
@endsection
@push('javascript')
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script>
  var swiper = new Swiper(".mySwiper", {
    cssMode: true,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    pagination: {
      el: ".swiper-pagination",
    },
    mousewheel: true,
    keyboard: true,
  });
</script>
@endpush
