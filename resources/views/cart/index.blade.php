@extends('layouts.cart')

@section('title', 'Cart')

@section('content')

    <table id="cart" class="table">
        <thead class="thead-dark">
        <tr>
            <th style="width:40%">Nama</th>
            <th style="width:10%">Jenis</th>
            <th style="width:10%">Price</th>
            <th style="width:8%">Qty</th>
            <th style="width:22%" class="text-center">Subtotal</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        @php $total = 0 @endphp
        @if(session('cart'))
            @foreach(session('cart') as $id => $details)
                @php $total += $details['price'] * $details['quantity'] @endphp
                <tr data-id="{{ $id }}">
                    <td data-th="Product">
                        <div class="row">
                            <div class="col-sm-9">
                                <h4 class="nomargin">{{ $details['name'] }}</h4>
                            </div>
                        </div>
                    </td>
                    <td data-th="Type">{{ $details['type'] }}</td>
                    <td data-th="Price">{{ rp_format($details['price']) }}</td>
                    <td data-th="Quantity">
                        @if(!str_contains($details['type'], 'Gazeboo'))
                            <input type="number" value="{{ $details['quantity'] }}" class="form-control quantity" onchange="updateChart(this.value, '{{ $id }}')" />
                        @endif
                    </td>
                    <td data-th="Subtotal" class="text-center">{{ rp_format($details['price'] * $details['quantity']) }}</td>
                    <td class="actions" data-th="">
                        <button class="btn btn-danger btn-sm" onclick="removeItem('{{ $id }}')"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
        <tfoot>
        <tr>
            <td colspan="5">
                <h3 class="float-end"><strong>Total {{ rp_format($total) }}</strong></h3>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                @if(session('cart'))
                    <form class="row" action="{{ route('cart.order') }}" method="post">
                        @csrf
                        <div class="col-md-2">
                            <h5 class="mt-2">Tanggal Booking</h5>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="time" class="form-control" name="booking_time" id="booking_time" required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="date" class="form-control" name="booking_date_start" id="booking_date" required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="date" class="form-control" name="booking_date_end" id="booking_date" required>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="float-end">
                                <button type="submit" class="btn btn-success">Checkout</button>
                            </div>
                        </div>
                    </form>
                @endif
            </td>
        </tr>
        </tfoot>
    </table>
    <a href="{{ url('/') }}" class="btn btn-warning float-end"><i class="fa fa-angle-left"></i> Continue Shopping</a>

@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script type="text/javascript">

    function updateChart(value, id) {
        $.ajax({
            url: '{{ route('update.cart') }}',
            method: "patch",
            data: {
                _token: '{{ csrf_token() }}',
                id: id,
                quantity: value
            },
            success: function (response) {
                window.location.reload();
            }
        });
    };

    function removeItem(id) {
        if(confirm("Are you sure want to remove?")) {
            $.ajax({
                url: '{{ route('remove.from.cart') }}',
                method: "DELETE",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: id
                },
                success: function (response) {
                    window.location.reload();
                }
            });
        }
    };

</script>
