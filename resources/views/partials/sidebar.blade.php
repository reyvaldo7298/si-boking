<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="">{{ config('app.name') }}</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="#">{{ strtoupper(substr(config('app.name'), 0, 2)) }}</a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">DASHBOARD</li>
        <li><a class="nav-link" href="{{ route('home') }}"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
        <li><a class="nav-link" target="_blank" href="{{ route('landingPage')}}"><i class="fas fa-scroll"></i> <span>Landing Page</span></a></li>

        <li class="menu-header">Master</li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-server"></i> <span>Core</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('gazeboo.index') }}"><span>Gazeboo</span></a></li>
                </ul>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('food.index') }}"><span>Makanan & Minuman</span></a></li>
                </ul>
            </li>

        <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-dollar-sign"></i> <span>Transaksi</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ route('booking.index') }}"><span>Booking Gazeboo</span></a></li>
            </ul>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ route('transaction.food.index') }}"><span>Booking Menu</span></a></li>
            </ul>
        </li>

        <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-paperclip"></i> <span>Laporan Gazeboo</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ route('report.gazeboo.finish') }}"><span>Selesai</span></a></li>
            </ul>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ route('report.gazeboo.decline') }}"><span>Ditolak</span></a></li>
            </ul>
        </li>

        <li><a class="nav-link" href="{{ route('service.index')}}"><i class="fas fa-scroll"></i> <span>Waktu Pelayanan</span></a></li>

{{--        <li><a class="nav-link" href="{{ route('server-busyness')}}"><i class="fas fa-scroll"></i> <span>Kesibukan Server</span></a></li>--}}

    </ul>
  </aside>
