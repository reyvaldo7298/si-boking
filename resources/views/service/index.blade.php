@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Waktu Pelayanan</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-right mb-4">
                            </div>

                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr style="white-space: nowrap">
                                            <th scope="col">#</th>
                                            <th scope="col">Gazeboo</th>
                                            <th scope="col">Booking ID</th>
                                            <th scope="col">Pemesan</th>
                                            <th scope="col">Tanggal Booking</th>
                                            <th scope="col">Waktu Pengunjung Pesan</th>
                                            <th scope="col">Waktu Admin Konfirmasi</th>
                                            <th scope="col">Waktu Yang Dibutuhkan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($bookings as $key => $booking)
                                            <tr>
                                                <td>{{ $key + 1 + (((request('page') ?? 1) - 1) * 10) }}</td>
                                                <td>{{ $booking->gazeboo->number }}</td>
                                                <td><span style="white-space: nowrap">{{ $booking->booking_id }}</span></td>
                                                <td><span style="white-space: nowrap">{{ @$booking->guest->name }}</span></td>
                                                <td style="white-space: nowrap">{{ parse_date($booking->booking_date) }}</td>
                                                <td style="white-space: nowrap">{{ parse_time_hm($booking->created_at) }}</td>
                                                <td style="white-space: nowrap">{{ parse_time_hm($booking->booking_time_approved) }}</td>
                                                <td style="white-space: nowrap">{{ $booking->serviceTime() }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="float-right">
                                {{ $bookings->appends(Request::except('page'))->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
