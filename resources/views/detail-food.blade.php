<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Janti Park</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  {{-- <link href="{{ asset('yummy/assets/img/favicon.png') }}" rel="icon"> --}}
  {{-- <link href="{{ asset('yummy/assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon"> --}}

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Amatic+SC:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Inter:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('yummy/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('yummy/assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
  <link href="{{ asset('yummy/assets/vendor/aos/aos.css') }}" rel="stylesheet">
  <link href="{{ asset('yummy/assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
  <link href="{{ asset('yummy/assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('yummy/assets/css/main.css') }}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Yummy
  * Updated: Mar 10 2023 with Bootstrap v5.2.3
  * Template URL: https://bootstrapmade.com/yummy-bootstrap-restaurant-website-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center justify-content-between">

      <a href="{{ route('landingPage') }}" class="logo d-flex align-items-center me-auto me-lg-0">
        <!-- Uncomment the line below if you also wish to use an image logo -->
        <!-- <img src="assets/img/logo.png" alt=""> -->
        <h1>JantiPark<span>.</span></h1>
      </a>

      <nav id="navbar" class="navbar">
        <ul>
{{--          <li><a href="#hero">Home</a></li>--}}
{{--          <li><a href="#about">About</a></li>--}}
{{--          <li><a href="#menu">Menu</a></li>--}}
          {{-- <li><a href="#gallery">Gallery</a></li>
          <li><a href="#contact">Contact</a></li> --}}
        </ul>
      </nav><!-- .navbar -->

      <a href="{{ route('cart') }}" class="btn btn-success rounded-pill">
          Cart
          <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge rounded-pill" style="background-color: yellowgreen !important;">{{ count((array) session('cart')) }}</span>
      </a>
        @if(Auth::user())
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <button class="btn-book-a-table" type="submit" style="border: none"><i class="fas fa-sign-out-alt"></i> Logout</button>
            </form>
        @else
            <a class="btn-book-a-table" href="{{ route('login') }}">Sign In</a>
        @endif
      <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
      <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>

    </div>
  </header><!-- End Header -->

  <main id="main">
    <!-- ======= Gallery Section ======= -->
      <section id="gallery" class="gallery section-bg">
        <div class="container mt-4" data-aos="fade-up">

          <div class="gallery-slider swiper mt-5">
            <div class="swiper-wrapper align-items-center">
                @foreach($food->pictures as $picture)
                    <div class="swiper-slide"><a class="glightbox" data-gallery="images-gallery" href="{{ $picture->url }}"><img src="{{ $picture->url }}" class="img-fluid" alt=""></a></div>
                @endforeach
            </div>
            <div class="swiper-pagination"></div>
          </div>

        </div>
      </section>
    <!-- End Gallery Section -->
  </main><!-- End #main -->

  <section id="about" class="about">
      <div class="container" data-aos="fade-up">

          <div class="section-header">
              <h2>{{ $food->type }}</h2>
              <p>{{ $food->name }}</p>
          </div>

          <div class="row gy-4">
              <div class="col-lg-12 d-flex align-items-end" data-aos="fade-up" data-aos-delay="300">
                  <div class="content ps-0 ps-lg-5">
                      <p>
                          {{ $food->description  }}
                      </p>
                      <h4>
                          Harga : {{ rp_format($food->price) }}
                      </h4>
                  </div>
              </div>
          </div>

      </div>
  </section>

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">

    <div class="container">
      <div class="row gy-3">
        <div class="col-lg-3 col-md-6 d-flex">
          <i class="bi bi-geo-alt icon"></i>
          <div>
            <h4>Alamat</h4>
            <p>
                Ngendo, Janti, Polanharjo, Klaten <br>
                Jawa Tengah<br>
            </p>
          </div>

        </div>

        <div class="col-lg-3 col-md-6 footer-links d-flex">
          <i class="bi bi-telephone icon"></i>
          <div>
            <h4>Reservations</h4>
            <p>
                <strong>Phone:</strong> 081215007979<br>
                <strong>Email:</strong> bumdesjantijaya@gmail.com<br>
            </p>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 footer-links d-flex">
          <i class="bi bi-clock icon"></i>
          <div>
            <h4>Opening Hours</h4>
            <p>
                <strong>Setiap Hari: 7.30</strong> - 15.30 WIB<br>
            </p>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 footer-links">
          <h4>Follow Us</h4>
          <div class="social-links d-flex">
            <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
            <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
            <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
            <a href="#" class="whatsapp"><i class="bi bi-whatsapp"></i></a>
          </div>
        </div>

      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>JantiPark</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/yummy-bootstrap-restaurant-website-template/ -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>

  </footer><!-- End Footer -->
  <!-- End Footer -->

  <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="{{ asset('yummy/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('yummy/assets/vendor/aos/aos.js') }}"></script>
  <script src="{{ asset('yummy/assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
  <script src="{{ asset('yummy/assets/vendor/purecounter/purecounter_vanilla.js') }}"></script>
  <script src="{{ asset('yummy/assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
  <script src="{{ asset('yummy/assets/vendor/php-email-form/validate.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('yummy/assets/js/main.js') }}"></script>

</body>

</html>
