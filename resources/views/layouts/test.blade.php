@extends('frontend.layouts.skeleton')

@section('frontend.layouts.app')

  <div class="main-wrapper">

    <header>
      @include('frontend.partials.header')
    </header>

    <!-- Main Content -->
    <div class="main-content">
      @yield('frontend.content')
    </div>

    <footer class="main-footer">
      @include('frontend.partials.footer')
    </footer>

  </div>
@endsection
